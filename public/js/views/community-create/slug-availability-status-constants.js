'use strict';

var slugAvailabilityStatusConstants = {
  PENDING: 'pending',
  AVAILABLE: 'available',
  GITHUB_CLASH: 'github-clash',
  AUTHENTICATION_FAILED: 'authentication-failed',
  UNAVAILABLE: 'unavailable',
  INVALID: 'invalid'
};

module.exports = slugAvailabilityStatusConstants;
